module Tests

open Complaints.Domain
open Complaints.PublicTypes
open Xunit
open System


let assertError (error: Errors) result = 
    match result with
    | Error e -> Assert.Equal(error, e)
    | _ -> failwith (result.ToString())

[<Fact>]
let ``Given new ComplaintCommand when Sale with same ProductId exist and SaleDate is in warranty time then Created event is produced`` () =
    let getSaleMock = fun _ -> Some {ProductId = "ProductIdMock"; Date = DateTime.UtcNow}
    let validCommand = { 
            ProductId = "ProductId"
            Description = "Description"
            Behest = Repair
            Customer = {
                FullName = "FullName"
                Street = "Street"
                Apartment ="Apartment"
                City = "City"
                ZipCode = "ZipCode"
                Phone = "Phone"
                Email = "Email" 
            }
        }

    let result = validateCreateCommand getSaleMock validCommand
    match result with
    | Ok e -> Assert.StrictEqual(validCommand, e)
    | _ -> failwith "Fail"
    
// [<Fact>]
// let ``Given new ComplaintCommand when Sale with same ProductId exist and SaleDate is NOT in warranty time then Error is returned`` () =
//     let getSaleMock = fun x -> Some {ProductId = "ProductIdMock"; Date = DateTime.UtcNow.AddYears(-2)}
//     let generateIdMock : GenerateComplaintId = (fun x -> "ComplaintMock")

//     createCommandHandler getSaleMock generateIdMock {ProductId = "ProductIdMock"}
//     |> assertError WarrantyExpired

// [<Fact>]
// let ``Given new ComplaintCommand when Sale with same ProductId exist and SaleDate is in warranty time then Created event is produced`` () =
//     let getSaleMock = fun x -> Some {ProductId = "ProductIdMock"; Date = DateTime.UtcNow}
//     let generateIdMock : GenerateComplaintId = (fun x -> "ComplaintIdMock")
    
//     let result = createCommandHandler getSaleMock generateIdMock {ProductId = "ProductIdMock"}
//     match result with
//     | Ok (ComplaintCreated e) -> 
//         Assert.Equal("ComplaintIdMock", e.ComplaintId)
//         Assert.Equal("ProductIdMock", e.Data.ProductId)
//     | _ -> failwith "Fail"
//     let complaintFromRepo = getComplaintImp "ComplaintIdMock"
//     match complaintFromRepo with
//     | Some ToValidation -> ignore()
//     | None -> failwith (repo.ToString())
//     | Some(value) -> failwith (value.ToString())
    
// [<Fact>]
// let ``Given new ComplaintCommand when Sale with same ProductId exist and SaleDate is NOT in warranty time then Error is returned`` () =
//     let getSaleMock = fun x -> Some {ProductId = "ProductIdMock"; Date = DateTime.UtcNow.AddYears(-2)}
//     let generateIdMock : GenerateComplaintId = (fun x -> "ComplaintMock")

//     createCommandHandler getSaleMock generateIdMock {ProductId = "ProductIdMock"}
//     |> assertError WarrantyExpired

// [<Fact>]
// let ``Given new ComplaintCommand when Sale with same ProductId NOT exist then Error is returned`` () =
//     let getSaleMock = fun x -> None 
//     let generateIdMock : GenerateComplaintId = (fun x -> "ComplaintMock")

//     createCommandHandler getSaleMock generateIdMock {ProductId = "ProductIdMock"}
//     |> assertError ProductIdNotFound



[<Fact>]
let ``Given Complaint To Validation When Cancel it then Canceled Event is produced`` () =
    let getComplaintMock : GetComplaint = fun (c:ComplaintId) -> Some ToValidation
    let result = cancelCommandHandler getComplaintMock {ComplaintId = "ComplaintIdMock"; Reason = Duplicate}
    match result with
    | Ok (ComplaintCanceled e) -> 
        Assert.Equal("ComplaintIdMock", e.ComplaintId)
        Assert.Equal(Duplicate, e.Data.Reason)
    | _ -> failwith "Fail"



[<Fact>]
let ``Given Complaint To Validation When Accept it then Accepted Event is produced`` () =
    let getComplaintMock : GetComplaint = fun (c:ComplaintId) -> Some ToValidation
    let result = acceptCommandHandler getComplaintMock {ComplaintId = "ComplaintIdMock"}
    match result with
    | Ok (ComplaintAccepted e) -> 
        Assert.Equal("ComplaintIdMock", e.ComplaintId)
        Assert.Equal("workerId", e.Data.WorkerId)
    | _ -> failwith "Fail"