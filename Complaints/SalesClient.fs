module SalesClient

open Complaints.PublicTypes
open Newtonsoft.Json
open System.Net.Http;

let getSale : GetSale = 
    fun productId ->
        let client = new HttpClient()
        let saleStream = client.GetStringAsync("http://localhost:5000/Sales?productId=" + productId) |> Async.AwaitTask |> Async.RunSynchronously
        let sale = JsonConvert.DeserializeObject<Sale>(saleStream)//.AsTask() |> Async.AwaitTask |> Async.RunSynchronously
        Some sale