namespace Complaints

open System
open System.Collections.Generic
open System.IO
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Serilog
open Serilog.Events
open Serilog.Sinks.Elasticsearch

module Program =
    let exitCode = 0

    let CreateHostBuilder args =
        Host
            .CreateDefaultBuilder(args)
            .UseSerilog(fun context services configuration ->
                configuration
                    .ReadFrom.Configuration(context.Configuration)
                    .ReadFrom.Services(services)
                    .Enrich.FromLogContext()
                    .WriteTo.Console()
                    .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                    .WriteTo.Elasticsearch(ElasticsearchSinkOptions(Uri("http://localhost:9200")))
                |> ignore)
            .ConfigureWebHostDefaults(fun webBuilder -> webBuilder.UseStartup<Startup>() |> ignore)

    [<EntryPoint>]
    let main args =
        Log.Logger <-
            LoggerConfiguration()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .WriteTo.Elasticsearch(ElasticsearchSinkOptions(Uri("http://localhost:9200")))
                .CreateBootstrapLogger()

        Log.Information("Starting web host")
        CreateHostBuilder(args).Build().Run()

        exitCode
