module Complaints.Contracts

open Complaints.PublicTypes

type CreateCommandDto = { 
    ProductId: string
    Type: int
    Description: string
    FullName: string
    Street: string
    Apartment: string
    City: string
    ZipCode: string
    Email: string
    Phone: string 
 }
module CreateCommandDto =
    let toCommand (dto: CreateCommandDto) : Create =
        let behest =
            match dto.Type with
            | 0 -> Repair
            | 1 -> NewProduct
            | 2 -> Discount
            | _ -> failwith "No such complaint type"

        { ProductId = dto.ProductId
          Description = dto.Description
          Behest = behest
          Customer =
              { FullName = dto.FullName
                Street = dto.Street
                Apartment = dto.Apartment
                City = dto.City
                ZipCode = dto.ZipCode
                Phone = dto.Phone
                Email = dto.Email } }

type CancelCommandDto = { ComplaintId: string; Reason: int }

module CancelCommandDto =
    let toCommand (dto: CancelCommandDto) : Cancel =
        match dto.Reason with
        | 0 ->
            { ComplaintId = dto.ComplaintId
              Reason = Duplicate }
        | 1 ->
            { ComplaintId = dto.ComplaintId
              Reason = Resignation }
        | _ -> failwith "No such reason type"

type AcceptCommandDto = { 
    ComplaintId: string 
}
module AcceptCommandDto =
    let toCommand (dto: AcceptCommandDto) : Accept = { ComplaintId = dto.ComplaintId }