module Complaints.PublicTypes
open System


type ProductId = string
type ComplaintId = string
type WorkerId = string
type Email = string
type Phone = string
type ZipCode = string

type CancelReason = 
    | Duplicate
    | Resignation
type RejectionReason = 
    | SomeReasonDontKnowYet
type BehestType =
    | Repair
    | NewProduct
    | Discount
    
type Sale = {
    ProductId: ProductId
    Date: DateTime
}

type Customer = {
    FullName: string
    Email: Email
    Phone: Phone
    Street: string
    City: string
    Apartment: string
    ZipCode: ZipCode
}

/////////COMMANDS
type Create = {
    ProductId: ProductId
    Behest: BehestType
    Description: string
    Customer: Customer
}

type Cancel = {
    ComplaintId: ComplaintId
    Reason: CancelReason
}

type Accept = {
    ComplaintId: ComplaintId
}

type Reject = {
    ComplaintId: ComplaintId
}

type ProposeSubstitute = {
    ComplaintId: ComplaintId
}

type Command = 
    | CreateComplaint of Create
    | CancelComplaint of Cancel
    | AcceptComplaint of Accept
    | RejectComplaint of Reject
    | ProposeSubstitute of ProposeSubstitute



///////////EVENTS
type ComplaintCreated = {
    ProductId: ProductId
    Type: BehestType
    Description: string
    Customer: Customer
}

type ComplaintCanceled = {
    Reason: CancelReason
}

type ComplaintAccepted = {
    WorkerId: WorkerId
}

type ComplaintRejected = {
    Reason: RejectionReason
}

type SubstituteProposed = {
    Proposed: BehestType
}

type ProductRepaired = {
    WorkerId: WorkerId
}

type ClientAccepted = {
    Description: string
}


type Event<'a> = {
    ComplaintId: ComplaintId
    Data: 'a
}

type Event = 
    | ComplaintCreated of Event<ComplaintCreated>
    | ComplaintCanceled of Event<ComplaintCanceled>
    | ComplaintAccepted of Event<ComplaintAccepted>
    | ComplaintRejected of Event<ComplaintRejected>
    | SubstituteProposed of Event<SubstituteProposed>
    | ProductRepaired of Event<ProductRepaired>
    | ClientAccepted of Event<ClientAccepted>



///////////STATES
type ProcessedState =
    | Canceled
    | Rejected
    | ProductRepaired

type ComplaintState = 
    | Init
    | ToValidation
    | WaitingForClientResponse
    | ToProcess
    | Processed of ProcessedState
    static member fold state event =  
        match state with
        | Init -> 
            match event with
            | ComplaintCreated(_) -> ToValidation
            | _ -> failwith "Not possible"
        | ToValidation -> 
            match event with
            | ComplaintAccepted(_) -> ToProcess
            | ComplaintCanceled(_) -> Processed Canceled
            | ComplaintRejected(_) -> Processed Rejected
            | SubstituteProposed(_) -> WaitingForClientResponse
            | _ -> failwith "Not possible"
        | WaitingForClientResponse -> 
            match event with
            | ComplaintCanceled(_) -> Processed Canceled
            | ClientAccepted(_) -> ToProcess
            | _ -> failwith "Not possible"
        | ToProcess(_) -> 
            match event with
            | Event.ProductRepaired _ -> Processed ProductRepaired
            | _ -> failwith "Not possible"
        | Processed(_) -> failwith "Not possible"


type Errors = 
    | ProductIdNotFound
    | WarrantyExpired
    | InvalidState

type GetSale = ProductId -> Option<Sale>
type GetComplaint = ComplaintId -> Option<ComplaintState>
type GenerateComplaintId = Create -> ComplaintId
type CreateCommandHandler = 
    GetSale
     -> GenerateComplaintId
     -> Create 
     -> Result<Event, Errors>

type CancelCommandHandler = 
    GetComplaint
     -> Cancel
     -> Result<Event, Errors>

type AcceptCommandHandler =
    GetComplaint
     -> Accept
     -> Result<Event, Errors>