module Complaints.Domain

open Complaints.PublicTypes
open Complaints.Persistance
open System

let validateCreateCommand getSale =
    fun (c: Create) ->
        let sale = getSale c.ProductId
        match sale with
        | Some s ->
            if s.Date > DateTime.UtcNow.AddYears(-1) then
                Ok c
            else
                Error WarrantyExpired
        | None -> Error ProductIdNotFound

let createCommandHandler : CreateCommandHandler =
    fun getSale generateId createCommand ->
        createCommand
        |> validateCreateCommand getSale
        |> Result.map generateId
        |> Result.map
            (fun complaintId ->
                ComplaintCreated
                    { Data =
                          { ProductId = createCommand.ProductId
                            Description = createCommand.Description
                            Type = createCommand.Behest
                            Customer = createCommand.Customer }
                      ComplaintId = complaintId })
        |> Result.map saveEvent

let cancelCommandHandler : CancelCommandHandler =
    fun getComplaint cancelCommand ->
        let validateCommand =
            fun (c: Cancel) ->
                let complaint = getComplaint cancelCommand.ComplaintId
                match complaint with
                | Some ToValidation -> Ok c
                | _ -> Error InvalidState

        cancelCommand
        |> validateCommand
        |> Result.map
            (fun c ->
                ComplaintCanceled
                    { Data = { Reason = c.Reason }
                      ComplaintId = c.ComplaintId })
        |> Result.map saveEvent

let acceptCommandHandler : AcceptCommandHandler =
    fun getComplaint acceptCommand ->
        let validateCommand =
            fun (c: Accept) ->
                let complaint = getComplaint acceptCommand.ComplaintId
                match complaint with
                | Some ToValidation -> Ok c
                | _ -> Error InvalidState

        acceptCommand
        |> validateCommand
        |> Result.map
            (fun c ->
                ComplaintAccepted
                    { Data = { WorkerId = "workerId" }
                      ComplaintId = c.ComplaintId })
        |> Result.map saveEvent