module Complaints.View

open Complaints.Persistance
open Complaints.PublicTypes

let getAllComplaints () =
    repo
    |> Map.map (fun c b -> List.fold ComplaintState.fold Init b)
