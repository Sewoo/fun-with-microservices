module Complaints.Persistance

open Complaints.PublicTypes

let mutable repo = Map.empty<ComplaintId, Event list>

let saveEvent event = 
    let changeFunction = fun eventList -> 
            match eventList with
            | Some w -> Some (List.append w [event])
            | _ -> failwith "List is required"
    let save e =
        if repo.ContainsKey e.ComplaintId 
        then repo <- Map.change e.ComplaintId changeFunction repo
        else repo <- Map.add e.ComplaintId [event] repo

    match event with
    | ComplaintCreated e -> save e
    | ComplaintCanceled e -> save e
    | ComplaintAccepted e -> save e
    | ComplaintRejected e -> save e
    | SubstituteProposed e -> save e
    | Event.ProductRepaired e -> save e
    | ClientAccepted e -> save e
    event

let getComplaintImp id = 
    if repo.ContainsKey id then Some (List.fold ComplaintState.fold Init repo.[id])
    else None