﻿namespace Complaints.Controllers

open System
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open Complaints.Contracts
open Complaints.PublicTypes
open Complaints.View

[<ApiController>]
[<Route("[controller]")>]
type ComplaintsController(logger: ILogger<ComplaintsController>) =
    inherit ControllerBase()

    let ResultToHttp (r: Result<Event, Errors>) : ActionResult = 
        match r with
         | Error ProductIdNotFound -> upcast NotFoundObjectResult()
         | Ok(resultValue) -> upcast OkObjectResult(resultValue)
         | Error e -> upcast UnauthorizedObjectResult()

    [<HttpGet>]
    member _.Get() = Complaints.View.getAllComplaints()

    [<HttpPost>]
    member _.Post(createCommand: CreateCommandDto) =
        let generateId _ = Guid.NewGuid().ToString()

        createCommand
        |> CreateCommandDto.toCommand
        |> Complaints.Domain.createCommandHandler SalesClient.getSale generateId
        |> ResultToHttp

    [<HttpDelete>]
    member _.Delete(cancelCommand: CancelCommandDto) =
        cancelCommand
        |> CancelCommandDto.toCommand
        |> Complaints.Domain.cancelCommandHandler Complaints.Persistance.getComplaintImp
