using System;

namespace Store
{
    public class Sale
    {
        public Guid ProductId { get; set; }

        public DateTime Date { get; set; }
    }
}
