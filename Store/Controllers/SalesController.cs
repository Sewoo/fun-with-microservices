﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Store.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SalesController : ControllerBase
    {
        static List<Sale> _sales = new List<Sale>();

        [HttpGet]
        public IActionResult Get(Guid productId)
        {
            var sale = _sales.FirstOrDefault(s => s.ProductId == productId);
            if (sale != null) return Ok(sale);
            return NotFound();
        }

        [HttpPost]
        public IActionResult Post(Sale sale)
        {
            var saleFromDb = _sales.FirstOrDefault(s => s.ProductId == sale.ProductId);
            if (saleFromDb != null) return Conflict();
            _sales.Add(sale);
            return Ok();
        }
    }
}
